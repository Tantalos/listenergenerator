package de.tantalos.sourcegenerator;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;

import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.Plugin;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.collect.Sets;
import com.sun.codemodel.JBlock;
import com.sun.codemodel.JClass;
import com.sun.codemodel.JCodeModel;
import com.sun.codemodel.JDefinedClass;
import com.sun.codemodel.JDocComment;
import com.sun.codemodel.JMethod;
import com.sun.codemodel.JMod;
import com.sun.codemodel.JPackage;

import de.tantalos.eventlistener.EventCallback;
import de.tantalos.eventlistener.EventListener;

public class Generate {

    public static final String LISTENER_METHOD_NAME = "onEventTriggered";

    public static final Set<Class<?>> classes = Sets.newHashSet();

    public static void main(String[] args) throws IOException {
        iwas();
        createTargetDir();
        for (Class<?> clazz : classes) {
            if (Event.class.isAssignableFrom(clazz)) {
                if (Event.class == clazz) {
                    continue;
                }
                generateEventListener((Class<? extends Event>) clazz);
            }
        }
    }

	private static void createTargetDir() {
		File f = new File("generated/src/main/java");
		try {
			if (f.mkdirs())
				System.out.println("Directory Created");
			else
				System.out.println("Directory is not created");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void collectClassesInClasspath(URL url) {
        File root = new File(url.getPath());
        for (File file : root.listFiles()) {
            if (file.isDirectory()) {
                System.out.println("file " + file + " is directory. go deeper");
                try {
                    collectClassesInClasspath(file.toURL());
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                }
            } else {
                String name = file.getName();
                System.out.println(name);
            }
        }
    }

    public static List<URL> getRootUrls() {
        List<URL> result = new ArrayList<URL>();

        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        while (cl != null) {
            if (cl instanceof URLClassLoader) {
                URL[] urls = ((URLClassLoader) cl).getURLs();
                result.addAll(Arrays.asList(urls));
            }
            cl = cl.getParent();
        }
        return result;
    }

    public static void iwas() throws IOException {
        for (URL url : getRootUrls()) {
            File f = new File(url.getPath());
            if (f.isDirectory()) {
                // visitFile(f);
            } else {
                visitJar(url);
            }
        }
    }

    static void visitJar(URL url) throws IOException {

        try (InputStream urlIn = url.openStream(); JarInputStream jarIn = new JarInputStream(urlIn)) {
            JarEntry entry;
            while ((entry = jarIn.getNextJarEntry()) != null) {
                if (entry.getName().endsWith(".class")) {
                    // System.out.println("FROM JAR: " + entry.getName());
                    try {
                        String className = entry.getName().replace("/", ".").substring(0, entry.getName().length() - 6);
                        System.out.println(className);
                        classes.add(Class.forName(className));
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }


    public static void generateEventListener(Class<? extends Event> eventClass) {
        try {
            Preconditions.checkArgument(Event.class.isAssignableFrom(eventClass));
            final String eventSimpleName = eventClass.getSimpleName();
            // i was lazy. simple hack to lower case the first letter
            final String eventParameterName = eventSimpleName.substring(0, 1).toLowerCase()
                    + eventSimpleName.substring(1, eventSimpleName.length() - 1);

            JCodeModel javaCodeModel = new JCodeModel();
            JClass eventListener = javaCodeModel.ref(EventListener.class);

            JPackage jp = javaCodeModel._package("de.tantalos.eventlistener");


            /* Giving Class Name to Generate */
            JDefinedClass javaClass = jp._class(eventClass.getSimpleName() + "Listener");
            javaClass._extends(eventListener.narrow(eventClass));


            // /* Adding class level coment */
            JDocComment jDocComment = javaClass.javadoc();
            jDocComment.add("This is a listener which listens to " + eventClass + " events");

            buildOnEventTriggered(javaClass, eventClass, eventParameterName);
            buildConstructor3(javaCodeModel, javaClass, eventClass, eventParameterName);
            buildConstructor2(javaCodeModel, javaClass, eventClass, eventParameterName);

            /* Building class at given location */
            javaCodeModel.build(new File("generated/src/main/java"));
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static JMethod buildOnEventTriggered(JDefinedClass javaClass, Class<? extends Event> eventClass,
            String eventParameterName) {
        JMethod onEventTriggered = javaClass.method(JMod.PUBLIC, void.class, LISTENER_METHOD_NAME);

        onEventTriggered.annotate(Override.class);
        onEventTriggered.annotate(EventHandler.class);

        /* Defining method parameter */
        onEventTriggered.param(eventClass, eventParameterName);

        /* Adding method body */
        JBlock jBlock = onEventTriggered.body();

        /* Defining statement */
        jBlock.directStatement("super." + LISTENER_METHOD_NAME + "(" + eventParameterName + ");");
        return onEventTriggered;
    }

    private static JMethod buildConstructor3(JCodeModel codeModel, JDefinedClass javaClass,
            Class<? extends Event> eventClass, String eventParameterName) {
        final String paramPluginName = "plugin";
        final String paramPredicateName = "predicate";
        final String paramCallbackName = "callback";

        JMethod constructor = javaClass.constructor(JMod.PUBLIC);

        JClass genericPredicate = codeModel.ref(Predicate.class).narrow(eventClass);
        JClass genericCallback = codeModel.ref(EventCallback.class).narrow(eventClass);

        constructor.param(Plugin.class, paramPluginName);
        constructor.param(genericPredicate, paramPredicateName);
        constructor.param(genericCallback, paramCallbackName);

        JBlock constBody = constructor.body();

        constBody.directStatement(
                "super(" + paramPluginName + ", " + paramPredicateName + ", " + paramCallbackName + ");");

        return constructor;

    }

    private static JMethod buildConstructor2(JCodeModel codeModel, JDefinedClass javaClass,
            Class<? extends Event> eventClass, String eventParameterName) {
        final String paramPluginName = "plugin";
        final String paramCallbackName = "callback";

        JMethod constructor = javaClass.constructor(JMod.PUBLIC);

        JClass genericCallback = codeModel.ref(EventCallback.class).narrow(eventClass);

        constructor.param(Plugin.class, paramPluginName);
        constructor.param(genericCallback, paramCallbackName);

        JBlock constBody = constructor.body();

        constBody.directStatement("super(" + paramPluginName + ", " + paramCallbackName + ");");

        return constructor;
    }

}
